# Youtube Player in Flowplayer Jacket

Open source Youtube Chromeless player wrapper developed with Adobe Flash Builder and ActionScript 3. Requires Flex SDK 4.0 (higher SDK will not work) and Flash player 10.0 or above. Fully functional and having a limited Javascript API.
