package
{
	public class Utils
	{
		public function Utils()
		{
		}
		
		/**
		 * Convert an integer to string timecode
		 * @param Number time code
		 * @return String time code
		 */
		public static function secondsToTimeString(secs:Number):String {
			var minutes:int = Math.floor(secs / 60);				
			var seconds:int = Math.ceil(secs % 60);
			var return_value:String = "";
			
			if(minutes < 10) return_value += "0";
			return_value += minutes.toString();
			
			return_value += ":";
			
			if(seconds < 10) return_value += "0";
			return_value += seconds.toString();
			
			return return_value;
		}
	}
}