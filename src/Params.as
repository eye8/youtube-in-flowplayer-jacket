package
{
	public class Params
	{
		// Player & video
		public static const DEFAULT_VID:String = "0QRO3gKj3qw";
		public static const YTPLAYER_URL:String = "http://www.youtube.com/apiplayer?version=3&enablejsapi=1";
		public static const FS_BTN_MAX2MIN:String = "assets/fsbtn.swf?type=max2min";
		public static const FS_BTN_MIN2MAX:String = "assets/fsbtn.swf?type=min2max";

		// Component dimensions
		public static const APP_WIDTH:int = 520;
		public static const APP_HEIGHT:int = 417;
		public static const CONTROLBAR_HEIGHT:Number = 27;
		public static const CONTROLS_ICON_WIDTH:Number = 12;
		public static const CONTROLS_ICON_HEIGHT:Number = 14;
		public static const VOLUME_SLIDER_WIDTH:Number = 90;
				
		// Custom events
		public static const PLAYER_STATE_CHANGE_EVENT:String = "onStateChange";
		//public static const PLAYER_READY_EVENT:String = "onPlayerReady";
		public static const PLAYER_LOADER_INITIALIZED_EVENT:String = "onPlayerLoaderInitialized";
		public static const PLAY_BTN_CLICKED_EVENT:String = "onPlayBtnClicked";
		public static const PLAY_SLIDER_CHANGED_EVENT:String = "onPlaySliderChanged";
		public static const VOLUME_BTN_CLICKED_EVENT:String = "onVolumeBtnClicked";
		public static const VOLUME_SLIDER_CHANGED_EVENT:String = "onVolumeSliderChanged";
		public static const FULLSCREEN_BTN_CLICKED_EVENT:String = "onFullscreenBtnClicked";
		public static const PLAYER_INTERACTED_EVENT:String = "onPlayerInteracted";
		public static const PLAYER_DEINTERACTED_EVENT:String = "onPlayerDeinteracted";
		
		// Player status
		public static const STATE_UNSTARTED:Number = -1;
		public static const STATE_ENDED:Number = 0;
		public static const STATE_PLAYING:Number = 1;
		public static const STATE_PAUSED:Number = 2;
		public static const STATE_BUFFERING:Number = 3;
		public static const STATE_CUED:Number = 5;
		
		// Other settings
		public static const DEFAULT_PLAYER_VOLUME:Number = 100;
		public static const UPDATE_PLAY_TIME_INTERVAL:Number = 500;
		public static const DOUBLE_CLICK_INTERVAL:Number = 250; // Double click time span for toggling fullscreen
		public static const AUTOHIDE_CONTROLBAR_DELAY:Number = 1000;
		public static const SHOW_CURSOR_TIME:Number = 0.5;
		public static const HIDE_CURSOR_TIME:Number = 0.25;
		public static const DEACTIVATE_CURSOR_LATENCY:Number = 2000;
		public static const API_READY_CALLBACK_FN_NAME:String = "onVialoguesPlayerReady";
		
		public static const EDLAB_URL:String = "http://edlab.tc.columbia.edu/";
	}
}