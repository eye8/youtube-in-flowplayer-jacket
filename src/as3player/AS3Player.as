package as3player
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.system.Security;
	
	import mx.containers.Canvas;
	import mx.controls.SWFLoader;
	
	public class AS3Player extends Canvas {
		// Member variables.
		private var _player:Object;
		private var _playerLoader:SWFLoader;
		private var _vurl:String;
		private var _startSeconds:Number = 0;
		
		// CONSTANTS.
		private static const PLAYER_URL:String =
			"http://www.youtube.com/apiplayer?version=3";
		private static const SECURITY_DOMAINS:Array = [
			"http://www.youtube.com", 
			"http://s.ytimg.com", 
			"http://i1.ytimg.com",
			"http://i2.ytimg.com",
			"http://i3.ytimg.com",
			"http://i4.ytimg.com",
		];
		private static const YOUTUBE_API_PREFIX:String = "http://gdata.youtube.com/feeds/api/videos/";
		
		/**
		 * Constructor method
		 */
		public function AS3Player():void {
			
			// Specifically allow the chromless player .swf access to our .swf.
			for(var domain_allowed:String in SECURITY_DOMAINS){
				Security.allowDomain(domain_allowed);				
			}
			
			_playerLoader = new SWFLoader();
			_playerLoader.addEventListener(Event.INIT, playerLoaderInitHandler);
			_playerLoader.load(PLAYER_URL);	
		}
		
		/**
		 * Callback function when the player loader is ready
		 * Init player loader
		 */
		private function playerLoaderInitHandler(event:Event):void {
			
			addChild(_playerLoader);
			
			// Define _player
			_player = _playerLoader.content;
						
			_playerLoader.content.addEventListener("onReady", onPlayerReady);
			_playerLoader.content.addEventListener("onError", onPlayerError);
						
			// On every mouse event, a PLAYER_INTERACTED_EVENT is dispatched to activate, and schedule to deactivate, the video controlbar
			_playerLoader.content.addEventListener(MouseEvent.MOUSE_MOVE,function(e:MouseEvent):void{
				dispatchEvent(new Event(Params.PLAYER_INTERACTED_EVENT,true));
			});
			
			_playerLoader.content.addEventListener(MouseEvent.ROLL_OVER,function(e:MouseEvent):void{
				dispatchEvent(new Event(Params.PLAYER_INTERACTED_EVENT,true));
			});
			
			_playerLoader.content.addEventListener(MouseEvent.ROLL_OUT,function(e:MouseEvent):void{
				dispatchEvent(new Event(Params.PLAYER_INTERACTED_EVENT,true));
			});
			
			_playerLoader.content.addEventListener(MouseEvent.CLICK,function(e:MouseEvent):void{
				dispatchEvent(new Event(Params.PLAYER_INTERACTED_EVENT,true));
			});
			
			_playerLoader.mouseEnabled = true;
			_playerLoader.mouseChildren = true;
			
			// Notify application that the player is ready to be interacted with
			dispatchEvent(new Event(Params.PLAYER_LOADER_INITIALIZED_EVENT));
		}
		
		/**
		 * Callback function when the youtube player is ready
		 * Init youtube player
		 */
		private function onPlayerReady(event:Event):void {			
			_player.cueVideoByUrl(_vurl,_startSeconds);
		}
		
		/**
		 * Callback function when an player error occurs
		 */
		private function onPlayerError(event:Event):void {
			trace("Player error:", Object(event).data);
		}
		
		/**
		 * Dynamically call any function through Youtube API
		 * By using this function, _player can be kept private, while the main application can still access any function in the Youtube API
		 */
		public function callYoutubeAPI(cmd:String, ...params):*{
			
			var results:*;
			
			try {
				results = _player[cmd].apply(NaN,params);
			} catch(e:Error){
				trace("Youtube API error: " + e.message + "\n" + cmd + "\n" + params.toString());
			}
			
			return results;
		}
		
		// Getter / Setters
		
		/**
		 * Set video id
		 */
		public function set vurl(v:String):void {
			_vurl = v;
		}
		
		/**
		 * Get current video id
		 */
		public function get vurl():String {
			return _vurl;
		}
		
		/**
		 * Set start time
		 */
		public function set startSeconds(s:Number):void{
			_startSeconds = s;
		}
		
		/**
		 * Get start time
		 */
		public function get startSeconds():Number {
			return _startSeconds;
		}
		
		public function get playerLoader():SWFLoader {
			return _playerLoader;
		}
	}
}